import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class DesafioFinal {
    private WebDriver driver;

    @Before
    public void abrir(){
        //set onde está o geckodriver
        System.setProperty("webdriver.gecko.driver", "C:/driver/geckodriver.exe");
        //instanciando o objeto driver
        driver = new FirefoxDriver();
        //maximiza a tela
        driver.manage().window().maximize();
        //link do site para o teste
        driver.get("https://testpages.herokuapp.com/styled/basic-html-form-test.html");
    }
    @After
    public void sair(){
        driver.quit();
    }

    @Test
    public void preencheFormulario() throws InterruptedException {
        //digita o usuario
        driver.findElement(By.xpath("/html/body/div/div[3]/form/table/tbody/tr[1]/td/input")).sendKeys("admin");
        //digita a senha
        driver.findElement(By.xpath("/html/body/div/div[3]/form/table/tbody/tr[2]/td/input")).sendKeys("admin");
        //faz um comentario
        driver.findElement(By.xpath("/html/body/div/div[3]/form/table/tbody/tr[3]/td/textarea")).clear();
        driver.findElement(By.xpath("/html/body/div/div[3]/form/table/tbody/tr[3]/td/textarea")).sendKeys("Hello World!");

        //marcar apenas a checkbox 3

        //verificando se a check box 1 foi selecionada
        if(driver.findElement(By.xpath("/html/body/div/div[3]/form/table/tbody/tr[5]/td/input[1]")).isSelected()){
            //se foi ela será desmarcada
            driver.findElement(By.xpath("/html/body/div/div[3]/form/table/tbody/tr[5]/td/input[1]")).clear();
        }
        //verificando se a check box 2 foi selecionada
        if(driver.findElement(By.xpath("/html/body/div/div[3]/form/table/tbody/tr[5]/td/input[2]")).isSelected()){
            //se foi ela será desmarcada
            driver.findElement(By.xpath("/html/body/div/div[3]/form/table/tbody/tr[5]/td/input[2]")).clear();
        }
        //verificando e a checkbox 3 foi selecionada, se estiver não faz nada, se não estiver marca
        if(!driver.findElement(By.xpath("/html/body/div/div[3]/form/table/tbody/tr[5]/td/input[3]")).isSelected()){
            driver.findElement(By.xpath("/html/body/div/div[3]/form/table/tbody/tr[5]/td/input[3]")).click();
        }

        //marca o radio 3
        driver.findElement(By.xpath("/html/body/div/div[3]/form/table/tbody/tr[6]/td/input[3]")).click();

        //selecionar apenas o item 3
        //verfica se o item 1 esta selecionado
        if(driver.findElement(By.xpath("/html/body/div/div[3]/form/table/tbody/tr[7]/td/select/option[1]")).isSelected()){
            //se foi ela será desmarcada
            driver.findElement(By.xpath("/html/body/div/div[3]/form/table/tbody/tr[7]/td/select/option[1]")).click();
        }
        //verfica se o item 2 esta selecionado
        if(driver.findElement(By.xpath("/html/body/div/div[3]/form/table/tbody/tr[7]/td/select/option[2]")).isSelected()){
            //se foi ela será desmarcada
            driver.findElement(By.xpath("/html/body/div/div[3]/form/table/tbody/tr[7]/td/select/option[2]")).click();
        }
        //verfica se o item 3 esta selecionado, se estiver não faz nada, se não estiver marca
        if(!driver.findElement(By.xpath("/html/body/div/div[3]/form/table/tbody/tr[7]/td/select/option[3]")).isSelected()) {
            driver.findElement(By.xpath("/html/body/div/div[3]/form/table/tbody/tr[7]/td/select/option[3]")).click();
        }

        //verifica se o item 4 esta selecionado
        if(driver.findElement(By.xpath("/html/body/div/div[3]/form/table/tbody/tr[7]/td/select/option[4]")).isSelected()) {
            //se foi ela será desmarcada
            driver.findElement(By.xpath("/html/body/div/div[3]/form/table/tbody/tr[7]/td/select/option[4]")).click();
        }

        //submit
        driver.findElement(By.xpath("/html/body/div/div[3]/form/table/tbody/tr[9]/td/input[2]")).click();

        //VALIDACÕES
        Thread.sleep(10000);
        //valida o username
        Assert.assertEquals("admin", driver.findElement(By.id("_valueusername")).getText());
        //valida o password
        Assert.assertEquals("admin", driver.findElement(By.id("_valuepassword")).getText());
        //valida o comentario
        Assert.assertEquals("Hello World!", driver.findElement(By.id("_valuecomments")).getText());
        //valida checkbox
        Assert.assertEquals("cb3", driver.findElement(By.id("_valuecheckboxes0")).getText());
        //valida radio
        Assert.assertEquals("rd3", driver.findElement(By.id("_valueradioval")).getText());
        //valida escolha
        Assert.assertEquals("ms3", driver.findElement(By.id("_valuemultipleselect0")).getText());

    }
}
